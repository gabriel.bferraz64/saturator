# Plugin VST de Saturação

Plugin feito com JUCE para servir de exemplo básico de VST, é um plugin de saturação com controles semelhates a um Tubescreamer (ganho, tone e volume).

## Build

Para compilar o plugin basta executar esses comandos em ordem.

```
cmake -S . -B build
```

```
cmake --build build
```