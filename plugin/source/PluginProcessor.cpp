#include "Utils/Saturator.h"
#include "Saturator/PluginProcessor.h"
#include "Saturator/PluginEditor.h"

//==============================================================================
// Construtor do PluginProcessor, também inicializa o construtor da AudioProcessorValueTreeState
// e dos outros objetos do JUCE que foram utilizados
AudioPluginAudioProcessor::AudioPluginAudioProcessor()
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  juce::AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", juce::AudioChannelSet::stereo(), true)
                     #endif
                       ),
        treeState(*this, nullptr, "PARAMS", createParameterLayout()),
        toneFilter(juce::dsp::IIR::Coefficients<float>::makeFirstOrderLowPass(44100, 20000.f)),
        output()
{
}

AudioPluginAudioProcessor::~AudioPluginAudioProcessor()
{
}

// Cria um layout de parametros que serão exibidos na interface
// Aqui você pode definir valores máximos, mínimos e o padrão
juce::AudioProcessorValueTreeState::ParameterLayout AudioPluginAudioProcessor::createParameterLayout()
{
    std::vector<std::unique_ptr<juce::RangedAudioParameter>> params;

    auto pGain = std::make_unique<juce::AudioParameterFloat>("Gain", "Gain", .0f, 16.f, .9f);
    params.push_back(std::move(pGain));

    auto pOutput = std::make_unique<juce::AudioParameterFloat>("Output", "Output", .0f, 1.f, 0.5f);
    params.push_back(std::move(pOutput));

    auto pTone = std::make_unique<juce::AudioParameterFloat>("Tone", "Tone", 800.f, 20000.f, 20000.f);
    params.push_back(std::move(pTone));

    return { params.begin(), params.end() };
}

//==============================================================================
const juce::String AudioPluginAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool AudioPluginAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool AudioPluginAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool AudioPluginAudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double AudioPluginAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int AudioPluginAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int AudioPluginAudioProcessor::getCurrentProgram()
{
    return 0;
}

void AudioPluginAudioProcessor::setCurrentProgram (int index)
{
    juce::ignoreUnused (index);
}

const juce::String AudioPluginAudioProcessor::getProgramName (int index)
{
    juce::ignoreUnused (index);
    return {};
}

void AudioPluginAudioProcessor::changeProgramName (int index, const juce::String& newName)
{
    juce::ignoreUnused (index, newName);
}

//==============================================================================
void AudioPluginAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    juce::ignoreUnused (sampleRate, samplesPerBlock);
    lastSampleRate = sampleRate;

    // Define um ProcessSpec, um objeto que armazena características da configuração de áudio
    // Se o objeto do JUCE possui um método prepare(), você vai precisar definir um ProcessSpec para utiliza-lo
    juce::dsp::ProcessSpec spec;
    spec.sampleRate = sampleRate;
    spec.maximumBlockSize = samplesPerBlock;
    spec.numChannels = getTotalNumOutputChannels();

    toneFilter.prepare(spec);
    toneFilter.reset();

    output.prepare(spec);
    output.reset();
}

void AudioPluginAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

bool AudioPluginAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    juce::ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    // Some plugin hosts, such as certain GarageBand versions, will only
    // load plugins that support stereo bus layouts.
    if (layouts.getMainOutputChannelSet() != juce::AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != juce::AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}

void AudioPluginAudioProcessor::updateParams()
{
    // Lê os valores dos parametros da interface e atualiza no código
    gain = *treeState.getRawParameterValue("Gain");

    float toneFreq = *treeState.getRawParameterValue("Tone");
    *toneFilter.state = *juce::dsp::IIR::Coefficients<float>::makeFirstOrderLowPass(lastSampleRate, toneFreq);

    float gainValue = *treeState.getRawParameterValue("Output");
    output.setGainLinear(gainValue);
}

void AudioPluginAudioProcessor::process(juce::dsp::ProcessContextReplacing<float> context)
{
    // Processamento utilizando os objetos do JUCE, primeiro o filtro, depois o controle de volume
    toneFilter.process(context);
    output.process(context);
}

void AudioPluginAudioProcessor::processBlock (juce::AudioBuffer<float>& buffer,
                                              juce::MidiBuffer& midiMessages)
{
    // Parte do template do JUCE, limpa o buffer antes de realizar os processamentos
    juce::ignoreUnused (midiMessages);

    juce::ScopedNoDenormals noDenormals;
    auto totalNumInputChannels  = getTotalNumInputChannels();
    auto totalNumOutputChannels = getTotalNumOutputChannels();

    for (auto i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, buffer.getNumSamples());
    
    // Lê os valores dos parametros na interface e atualiza no código
    updateParams();

    // Processamento inicia aqui, esse primeiro loop percorre cada canal de entrada do plugin
    for (int channel = 0; channel < totalNumInputChannels; ++channel)
    {
        // Ponteiro de escrita do array do canal de saída, iremos utilizar ele logo abaixo
        // para escrever valores no buffer de saída
        auto* channelData = buffer.getWritePointer (channel);
        juce::ignoreUnused (channelData);
        
        // Percorre cada sample do canal
        for (auto sample = 0; sample < buffer.getNumSamples(); ++sample)
        {
            // Aplica o ganho pré-saturação
            auto input = channelData[sample] * gain; 

            // Sobrescreve o sample no buffer com o valor saturado
            channelData[sample] = static_cast<float>(Utils::saturate(input));
        }
    }

    // Cria um bloco de áudio a partir do buffer e processa ele com os objetos do JUCE
    // Quando o objeto do JUCE tem como parametro um ProcessContextReplacing tu precisa fazer essa converção abaixo
    // Primeiro do AudioBuffer para um AudioBlock, e depois do AudioBlock para um ProcessContextReplacing
    juce::dsp::AudioBlock<float> block(buffer);
    process(juce::dsp::ProcessContextReplacing<float>(block));
}

//==============================================================================
bool AudioPluginAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

juce::AudioProcessorEditor* AudioPluginAudioProcessor::createEditor()
{
    return new juce::GenericAudioProcessorEditor(*this);
}

//==============================================================================
void AudioPluginAudioProcessor::getStateInformation (juce::MemoryBlock& destData)
{
    juce::ignoreUnused (destData);
}

void AudioPluginAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    juce::ignoreUnused (data, sizeInBytes);
}

//==============================================================================.
juce::AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new AudioPluginAudioProcessor();
}
