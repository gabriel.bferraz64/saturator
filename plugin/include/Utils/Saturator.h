#pragma once

#include <cmath>

namespace Utils {
    // Equação de saturação do sinal -> y(x) = tanh(x^3 + e^x - 1)
    template <typename T>
    T saturate(T in)
    {
        double s = pow(in, 3.f) + exp(in) - 1;
        return tanh(s);
    }
}